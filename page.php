<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package onegate_theme
 */

get_header(); ?>

<div class="intern-about">

	<? if ( have_posts() ) : ?>
		<header class="intern-header">
			<div class="uk-container uk-container-center">
				<? the_title('<h1>','</h1>');?>
			</div>
		</header><!-- .page-header -->

		<div class="uk-container uk-container-center">
				<? while ( have_posts() ) : the_post();?>
					<? the_content(); ?>
					<div class="uk-grid">
						<? if( have_rows() ):
							while ( have_rows() ) : the_row(); ?>
								<?the_content();?>

							<? endwhile;
					endif; ?>
					</div>

				<? endwhile;
			endif; ?>
			</div>
		</div>
</div><!-- about -->


<?php
get_footer();
