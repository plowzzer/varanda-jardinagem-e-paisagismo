<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package onegate_theme
 */

get_header(); ?>
  <header class="intern-header">
    <div class="uk-container uk-container-center">
      <? the_title('<h1>','</h1>');?>
    </div>
  </header><!-- .page-header -->

  <div class="uk-container uk-container-center">
		<div class="uk-grid">

			<?php	while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', get_post_format() );

			endwhile; // End of the loop.
			?>


		</div>
	</div><!-- #primary -->

<?php
get_footer();
