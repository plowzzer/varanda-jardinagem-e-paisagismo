<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package onegate_theme
 */

?>

<header class="entry-header">
	<?php
		if ( is_single() ) {
			the_title( '<h2 class="entry-title">', '</h2>' );
		} else {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}?>

	<div class="entry-meta">
		<?php if (pl_get_post_views(get_the_ID()) != null) :?>
			<p><small><i class="fa fa-eye"></i> <?php echo pl_get_post_views(get_the_ID()); ?> visualizações |
		<?php else: echo '<p>'; endif; ?>
		<i class="fa fa-calendar"></i> <?php the_time('j \d\e F \d\e Y') ?> |
		<?php
		if (the_category() != null ) {
			echo '<i class="fa fa-folder-open-o"></i> Caterogirias:';
			the_category(', ');
		} if (the_taxonomies() != null){
			echo '<i class="fa fa-folder-open-o"></i> ';
			the_taxonomies(', ');
		}?></small>
	</div><!-- .entry-meta -->
</header><!-- .entry-header -->
