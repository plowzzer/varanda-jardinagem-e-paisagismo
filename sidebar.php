<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package onegate_theme
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="sidebar">
	<?php if (get_option('support_link') != '') { ?>
		<a class="need-support" target="_blank" href="<?php echo get_option('support_link'); ?>">
			<h4><?php echo get_option('support_title'); ?></h4>
			<p><?php echo get_option('support_text'); ?></p>
		</a>
	<?php } ?>


	<ul class="counter">
		<h4>FAQ</h4>
		<?php
		$popularpost = new WP_Query( array( 'post_type' => array( 'ajuda' ),'posts_per_page' => 5, 'meta_key' => 'pl_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
		while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>

		<li>
			<?php echo pl_postformat_image($postID); ?>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br>
			<small>
				<i class="fa fa-eye"></i> <?php echo pl_get_post_views(get_the_ID()); ?>
				| <i class="fa fa-calendar"></i> <?php the_time('d/m/Y') ?>
			</small>
		</li>

		<?php endwhile;
		wp_reset_postdata();?>
	</ul>

	<ul class="categories">
		<h4>Categorias</h4>
		<?php $taxonomy = 'sos-category';
		$tax_terms = get_terms($taxonomy);
		foreach ($tax_terms as $tax_term) { ?>
			<?php echo '<li class="category-name">' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '>' . $tax_term->name.'</a></li>'; ?>
		<?php } ?>
	</ul>

	<div class="tag-cloud">
		<h4>Tags Populares</h4>
		<?php
			$args = array(
				'taxonomy' 	=> array( 'sos-tag' ),
				'separator' => " | ",
				'smallest'  => 10,
			);
			wp_tag_cloud( $args );
		?>
	</div>

	<!-- <aside id="secondary" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside>-->
</div>
