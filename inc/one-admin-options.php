<?php //This adds the theme admin options

add_action('admin_menu', 'add_global_custom_options');
function add_global_custom_options(){
	add_menu_page('Options', 'Options', 'manage_options', 'functions', 'theme_options');
  add_submenu_page( 'functions', 'Social', 'Social', 'manage_options', 'theme-op-settings', 'social_options');
  // add_submenu_page( 'functions', 'FAQ page title', 'FAQ menu label', 'manage_options', 'theme-op-faq', '2_options');
}

function theme_options() { ?>
  <div class="wrap">
    <h2>Theme Options</h2>
    <form method="post" action="options.php">
      <?php wp_nonce_field('update-options') ?>
			<h2>Footer</h2>
			<table>
				<tbody>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>JavaScript code:</strong><br>
								<span class="description">Paste your Google Analytics, other tracking code or any script you need.
								This will be loaded in the footer.</span>
							</div>
						</th>
						<td>
							<textarea name="js_script" rows="8" cols="45" value="<?php echo get_option('js_script'); ?>"></textarea>
						</td>
					</tr>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>Copyright text:</strong><br>
							</div>
						</th>
						<td>
							<input type="text" name="copyright" size="45" value="<?php echo get_option('copyright'); ?>" />
						</td>
					</tr>
				</tbody>
			</table>
			<hr>

      <p><input type="submit" name="Submit" value="Save Options" class="button"/></p>
      <input type="hidden" name="action" value="update" />
      <input type="hidden" name="page_options" value="js_script,copyright"/>
    </form>
  </div>
	<script>jQuery(document).ready(function() {
    var formfield;
    /* user clicks button on custom field, runs below code that opens new window */
    jQuery('.upload-button').click(function() {
        formfield = jQuery(this).prev('input#image_location'); //The input field that will hold the uploaded file url
        tb_show('','media-upload.php?TB_iframe=true');
        return false;
    });

    //adding my custom function with Thick box close function tb_close() .
    window.old_tb_remove = window.tb_remove;
    window.tb_remove = function() {
        window.old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
        formfield=null;
    };

    // user inserts file into post. only run custom if user started process using the above process
    // window.send_to_editor(html) is how wp would normally handle the received data

    window.original_send_to_editor = window.send_to_editor;
    window.send_to_editor = function(html){
        if (formfield) {
            fileurl = jQuery('img',html).attr('src');
            jQuery(formfield).val(fileurl);
            tb_remove();
        } else {
            window.original_send_to_editor(html);
        }
    };
});</script>
<?php }

function social_options() { ?>
  <div class="wrap">
    <h2>Social options</h2>
		<form method="post" action="options.php">
			<?php wp_nonce_field('update-options') ?>
			<table>
				<tbody>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>Email:</strong><br>
							</div>
						</th>
						<td>
							<input type="text" name="email" size="45" value="<?php echo get_option('email'); ?>" />
						</td>
					</tr>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>Email subject:</strong><br>
							</div>
						</th>
						<td>
							<input type="text" name="emailsubject" size="45" value="<?php echo get_option('emailsubject'); ?>" />
						</td>
					</tr>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>Twitter:</strong><br>
							</div>
						</th>
						<td>
							<input type="text" name="twitter" size="45" value="<?php echo get_option('twitter'); ?>" />
						</td>
					</tr>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>Facebook:</strong><br>
							</div>
						</th>
						<td>
							<input type="text" name="facebook" size="45" value="<?php echo get_option('facebook'); ?>" />
						</td>
					</tr>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>Instagram:</strong><br>
							</div>
						</th>
						<td>
							<input type="text" name="instagram" size="45" value="<?php echo get_option('instagram'); ?>" />
						</td>
					</tr>
					<tr valign="top">
						<td scope="row">
							<div>
								<strong>Facebook page plugin:</strong><br>
							</div>
						</th>
						<td>
							<textarea name="facebook_pageplugin" rows="8" cols="45" value="<?php echo get_option('facebook_pageplugin'); ?>"><?php echo get_option('facebook_pageplugin');?></textarea>
						</td>
					</tr>

				</tbody>
			</table>
      <p><input type="submit" name="Submit" value="Save Options" /></p>
      <input type="hidden" name="action" value="update" />
      <input type="hidden" name="page_options" value="email,emailsubject,twitter,facebook,facebook_pageplugin,instagram" />
    </form>
  </div>
<?php }
