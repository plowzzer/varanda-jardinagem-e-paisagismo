<?php
/**
*
*Template Name: Home
*Template texto: Pagina inicial, usar como index
*
* @package capela_novas
*/

get_header(); ?>
<div id="inicio">
  <?php if( have_rows('banner-imagens-home') ): ?>
    <div class="uk-slidenav-position normal-cover" data-uk-slideshow="{animation: 'scale',autoplay:true}">
      <ul class="uk-slideshow">
        <?php $counter = 0;
        while( have_rows('banner-imagens-home') ): the_row();
          $counter++;
        endwhile;?>
      	<?php while( have_rows('banner-imagens-home') ): the_row();
      		// vars
      		$imagem = get_sub_field('imagem');
      		$link = get_sub_field('link');
      		?>
  	      <li>
    			<?php if( $link ): ?>
    				<a href="<?php echo $link; ?>">
    			<?php endif; ?>
    				<img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt'] ?>" />
    			<?php if( $link ): ?>
    				</a>
    			<?php endif; ?>
      		</li>
    	<?php endwhile; ?>
    	</ul>
      <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
      <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
      <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
        <? for ($i=0; $i < $counter ; $i++) { ?>
          <li data-uk-slideshow-item="<?echo $i; ?>"><a href=""></a></li>
        <? } //endif ?>
      </ul>
    </div>
  <?php endif; ?>
</div>

<div class="about" id="quemsomos" data-uk-scrollspy="{cls:'uk-animation-fade'}">
  <div class="intern-about">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-2-3">
          <h1>Quem Somos</h1>
          <? the_field('quem_somos'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="services" id="servicos">
  <div class="intern-services">
    <div class="uk-container uk-container-center">
      <h1>Serviços</h1>
      <div class="uk-grid">
        <?php
        if( have_rows('serviços') ):
          // loop through the rows of data
            while ( have_rows('serviços') ) : the_row();
              //Var
              $image = get_sub_field('imagem');
              $icon = get_sub_field('icone');
              $title = get_sub_field('titulo');
              $description = get_sub_field('descricao');?>

              <div class="uk-width-1-1 uk-width-medium-1-3 service-single">
                <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" />
                <h3><? echo $title; ?></h3>
                <? echo $description; ?>
              </div>
            <?endwhile;
        endif;?>
      </div>
    </div>
  </div>
</div>

<div class="portfolio" id="portfolio">
  <div class="intern-portfolio">
      <h1>Portfolio</h1>
      <div class="uk-grid uk-grid-collapse">
        <?php
        if( have_rows('portfolio') ):
          // loop through the rows of data
          $count = 0;
            while ( have_rows('portfolio') ) : the_row();
              //Var
              $image = get_sub_field('imagem');
              $title = get_sub_field('titulo_portfolio');
              $description = get_sub_field('descricao');
              $count++; ?>
              <div class="uk-width-1-2 uk-width-medium-1-4 portfolio-single">
                <a href="#portfolio-modal-<? echo $count;?>" data-uk-modal>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $icon['alt'] ?>" />
                  <div class="title">
                    <h2><? echo $title; ?></h2>
                  </div>
                </a>
                <div id="portfolio-modal-<? echo $count;?>" class="uk-modal">
                    <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                        <a href="" class="uk-modal-close uk-close uk-close-alt"></a>
                        <img src="<?php echo $image['url']; ?>" alt="">
                        <div class="description">
                          <h3><? echo $title; ?></h3>
                          <? echo $description; ?>
                        </div>
                    </div>
                </div>
              </div>
            <?endwhile;
        endif;?>
    </div>
  </div>
</div>

<div class="contact" id="contato">
  <div class="intern-contact">
    <div class="uk-container uk-container-center">
      <h1>Contato</h1>
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <p><i class="fa fa-map-marker" aria-hidden="true"></i> <?the_field('endereco');?></p>
          <p><i class="fa fa-phone" aria-hidden="true"></i> <?the_field('telefone');?></p>
          <p><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?the_field('email');?>?subject=Contato"><?the_field('email');?></a></p>
        </div>
      </div>
      <? $location = get_field('mapa');
      if( !empty($location) ):?>
    </div>
    <div class="acf-map">
    	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
    </div>
    <div>
    <?php endif; ?>

    </div>
  </div>
</div>

<div class="facebook">
  <div class="intern-facebook">
    <a target="_blank" href="<? the_field('facebook-page'); ?>">Visite nossa Fanpage <img class="border" src="<?php bloginfo('template_url'); ?>/assets/img/facebook.svg" alt="Facebook da Varanda da Planta" /></a>
  </div>
</div>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyAHnAlDaZT6Q1n6YvnNYvGQdwYgrRuFmnw"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {

	// var
	var $markers = $el.find('.marker');


	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};


	// create map
	var map = new google.maps.Map( $el[0], args);


	// add a markers reference
	map.markers = [];


	// add markers
	$markers.each(function(){

    	add_marker( $(this), map );

	});


	// center map
	center_map( map );


	// return
	return map;

}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>

<? get_footer(); ?>
