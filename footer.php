<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package onegate_theme
 */

?>
	<footer role="contentinfo">
		<div class="uk-container uk-container-center">
			<div class="uk-grid uk-grid-match">
				<div class="menu uk-width-1-1">
					<ul>
						<li><a class="img" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img inline class="logo" src="<?php bloginfo('template_url'); ?>/assets/img/varanda_da_planta-logo-white.svg" alt="" /></a></li>
						<li><a href="#quemsomos" data-uk-smooth-scroll="{offset: 92}">Quem Somos</a></li>
						<li><a href="#servicos" data-uk-smooth-scroll="{offset: 92}">Serviços</a></li>
						<li><a href="#portfolio" data-uk-smooth-scroll="{offset: 120}">Portfolio</a></li>
						<li><a href="#contato" data-uk-smooth-scroll="{offset: 92}">Contato</a></li>
					</ul>
				</div>
				<div class="info uk-width-1-1">
					<p><i class="fa fa-map-marker" aria-hidden="true"></i> <?the_field('endereco');?><span class="gap"></span>
					<i class="fa fa-phone" aria-hidden="true"></i> <?the_field('telefone');?><span class="gap"></span>
					<i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?the_field('email');?>?subject=Contato"><?the_field('email');?></a></p>
				</div>
				<div class="copy uk-width-1-1">
					<p>Copyright (c) 2016 Varanda da Planta. Todos os direitos reservados.</p>
					<p>by: <a href="http://ocondado.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/img/ocondado.svg" alt="Agencia O Condado" /></a></p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
<?php get_template_part( 'template-parts/facebook' ); ?>
<?php get_template_part( 'template-parts/google' ); ?>

</script>
<?php wp_footer(); ?>

</body>
</html>
