<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package onegate_theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/fonts.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/uikit.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/components/slideshow.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/components/slidenav.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/components/dotnav.min.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!-- <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" /> -->

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/uikit.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/core/smooth-scroll.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/core/scrollspy.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/components/slideshow.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/components/slideshow-fx.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/components/lightbox.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/components/slideset.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/components/grid.min.js"></script>
	<!-- <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.fullPage.js"></script> -->
	<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false"></script> -->

  <!-- <script type="text/javascript">
		$(window).load(function() {
			$(".overlay").fadeOut(1500);
		});
	</script> -->

<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
	<header class="main <?php if ( is_front_page() ) { echo "home-header"; }?>">
		<div class="uk-container uk-container-center">
			<!-- <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></a> -->
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img inline class="logo" src="<?php bloginfo('template_url'); ?>/assets/img/varanda_da_planta-logo.svg" alt="" /></a>
				<nav id="menu" class="uk-hidden-small uk-hidden-medium">
					<ul data-uk-scrollspy-nav="{closest:'li'}">
						<li><a href="#quemsomos" data-uk-smooth-scroll="{offset: 92}">Quem Somos</a></li>
						<li><a href="#servicos" data-uk-smooth-scroll="{offset: 92}">Serviços</a></li>
						<li><a href="#contato" data-uk-smooth-scroll="{offset: 92}">Contato</a></li>
						<li><a href="#portfolio" data-uk-smooth-scroll="{offset: 120}">Portfolio</a></li>
					</ul>
					<a id="facebook" class="uk-hidden-small uk-hidden-medium" href="https://www.facebook.com/VarandadaPlantaPaisagismo" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/img/facebook.svg" alt="Facebook da Varanda da Planta" /></a>
						<?php //wp_nav_menu( array( 'theme_location' => 'left_menu', 'container' => false, 'menu_class' => 'uk-text-center menu_left', 'items_wrap' => '<ul id="%1$s" class="%2$s" data-uk-scrollspy-nav="{closest:' . "'li'" . ', smoothscroll:true}">%3$s</ul>') ); ?>
						<?php //wp_nav_menu( array( 'theme_location' => 'right_menu', 'container' => false, 'menu_class' => 'uk-text-center menu_right', 'items_wrap' => '<ul id="%1$s" class="%2$s" data-uk-scrollspy-nav="{closest:' . "'li'" . ', smoothscroll:true}">%3$s</ul>') ); ?>
				</nav>
				<a class="hamburguer uk-hidden-large" href="#menu-off" data-uk-offcanvas><i class="fa fa-bars"></i></a>
			</div>

		</div>

		<div id="menu-off" class="uk-offcanvas">
	    <div class="uk-offcanvas-bar uk-offcanvas-bar-flip green">
				<ul data-uk-scrollspy-nav="{closest:'li'}">
					<li><a href="#quemsomos" data-uk-smooth-scroll="{offset: 92}">Quem Somos</a></li>
					<li><a href="#servicos" data-uk-smooth-scroll="{offset: 92}">Serviços</a></li>
					<li><a href="#portfolio" data-uk-smooth-scroll="{offset: 120}">Portfolio</a></li>
					<li><a href="#contato" data-uk-smooth-scroll="{offset: 92}">Contato</a></li>
				</ul>
			</div>
		</div>
	</header>
